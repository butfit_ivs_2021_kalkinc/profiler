/*
 * Copyright (c) 2021. Dítě Jindřich, Lebedenko Anastasiia, Čuhanič Jakub,
 *  Hradil Michal
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cz.vutbr.fit.ivs._2021.kalkinc;

import com.google.common.collect.Lists;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.Calculator;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.CalculatorImpl;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.Token;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.TokenImpl;
import cz.vutbr.fit.ivs._2021.kalkinc.lib.parser.ParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Profiler {
    public static void main(String[] args) throws IOException, ParserException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Calculator calculator = new CalculatorImpl();
        String str = br.readLine();
        int N = 0;
        ArrayList<String> numbers = new ArrayList<>();
        while (str != null) {
            Arrays.stream(str.split(" ")).map(x -> x.split("\t")).flatMap(Arrays::stream).forEach(numbers::add);
            str = br.readLine();
        }
        N = numbers.size();
        if (numbers.size() == 0) {
            return;
        }
        ArrayList<Token> tokens = new ArrayList<>();
        tokens.add(new TokenImpl("1"));
        tokens.add(new TokenImpl("/"));
        tokens.add(new TokenImpl(Integer.toString(N)));
        tokens.add(new TokenImpl("*"));
        tokens.add(new TokenImpl("("));
        int countDownToNextParen = 50; // We need to parenthesize it a bit, otherwise it has a tendency to stack overflow for very long inputs
        if (numbers.size() > 51) {
            tokens.add(new TokenImpl("("));
        }
        for (int i = 0; i < numbers.size() - 1; i++) {
            tokens.add(new TokenImpl(numbers.get(i)));
            if (countDownToNextParen-- <= 0 && i != numbers.size() - 1) {
                tokens.add(new TokenImpl(")"));
                tokens.add(new TokenImpl("+"));
                tokens.add(new TokenImpl("("));
                countDownToNextParen = 50;
            } else {
                tokens.add(new TokenImpl("+"));
            }
        }
        tokens.add(new TokenImpl(numbers.get(numbers.size() - 1)));
        if (numbers.size() > 51) {
            tokens.add(new TokenImpl(")"));
        }
        tokens.add(new TokenImpl(")"));
        calculator.setExpression(tokens);
        tokens = Lists.newArrayList(new TokenImpl("M+"));
        calculator.setExpression(tokens);
        tokens = new ArrayList<>();
        tokens.add(new TokenImpl("2"));
        tokens.add(new TokenImpl("root"));
        tokens.add(new TokenImpl("("));
        tokens.add(new TokenImpl("1"));
        tokens.add(new TokenImpl("/"));
        tokens.add(new TokenImpl("("));
        tokens.add(new TokenImpl(Integer.toString(N)));
        tokens.add(new TokenImpl("-"));
        tokens.add(new TokenImpl("1"));
        tokens.add(new TokenImpl(")"));
        tokens.add(new TokenImpl("*"));
        tokens.add(new TokenImpl("("));
        countDownToNextParen = 50;
        if (numbers.size() > 51) {
            tokens.add(new TokenImpl("("));
        }
        for (int i = 0; i < numbers.size() - 1; i++) {
            tokens.add(new TokenImpl(numbers.get(i)));
            tokens.add(new TokenImpl("^"));
            tokens.add(new TokenImpl("2"));
            if (countDownToNextParen-- <= 0 && i != numbers.size() - 1) {
                tokens.add(new TokenImpl(")"));
                tokens.add(new TokenImpl("+"));
                tokens.add(new TokenImpl("("));
                countDownToNextParen = 50;
            } else {
                tokens.add(new TokenImpl("+"));
            }
        }
        tokens.add(new TokenImpl(numbers.get(numbers.size() - 1)));
        tokens.add(new TokenImpl("^"));
        tokens.add(new TokenImpl("2"));
        if (numbers.size() > 51) {
            tokens.add(new TokenImpl(")"));
        }
        tokens.add(new TokenImpl("-"));
        tokens.add(new TokenImpl(Integer.toString(N)));
        tokens.add(new TokenImpl("*"));
        tokens.add(new TokenImpl("M"));
        tokens.add(new TokenImpl("^"));
        tokens.add(new TokenImpl("2"));
        tokens.add(new TokenImpl(")"));
        tokens.add(new TokenImpl(")"));
        calculator.setExpression(tokens);
        System.out.println(calculator.getResult());
    }
}
